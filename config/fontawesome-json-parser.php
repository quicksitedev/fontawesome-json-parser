<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Fontawesome Json parser configurations
    |--------------------------------------------------------------------------
    |
    */

   'filename' => 'icons',

    'icons-route' => env('FONTAWESOME_JSON_ROUTE', '/fontawesome-icons'),
    'load-from-route' => env('FONTAWESOME_JSON_LOAD_FROM_ROUTE', 'fontawesome-icons'),
    'export-to-route' => env('FONTAWESOME_JSON_EXPORT_TO_ROUTE', 'fontawesome-icons'),
    'icons-route-index' => env('FONTAWESOME_ROUTE_INDEX', 'fontawesome-index'),
];
