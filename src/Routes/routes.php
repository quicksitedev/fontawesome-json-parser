<?php

/*
|--------------------------------------------------------------------------
| Fontawesome route
|--------------------------------------------------------------------------
|
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => 'fontawesome'], function () {
        Route::get(config('fontawesome-json-parser.icons-route-index'), 'Quicksite\FontawesomeJsonParser\Http\Controllers\JsonVisualiser\JsonVisualiserController@index')->name('fontawesome.icons.index');
    });
});
