<?php

namespace Quicksite\FontawesomeJsonParser\Console;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use JsonMachine\JsonMachine;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ProcessJsonCommand extends Command
{
    protected $signature = 'fontawesome:parse';

    protected $description = 'Parse the fontawesome jsons icons file. Version 5.13.0';

    public function handle()
    {
        if(!Storage::disk('local')->exists(config('fontawesome-json-parser.load-from-route') . DIRECTORY_SEPARATOR . config('fontawesome-json-parser.filename'))) {
            $file = __DIR__ . '/icons/icons.json';
        } else {
            $file = Storage::disk('local')->path(config('fontawesome-json-parser.load-from-route') . DIRECTORY_SEPARATOR . config('fontawesome-json-parser.filename'));
        }

        $json = JsonMachine::fromFile($file);

        $icons = [];

        foreach ($json as $name => $data) {
            foreach ($data['styles'] as $style) {
                $icons[$style][] = [
                    'title' =>  'fa' . $style[0]. ' fa-' . $name,
                    'searchTerms' => implode(',', $data['search']['terms']),
                ];
            }
        }

        $export = JsonMachine::fromFile($file);
        foreach ($export as $name => $data) {
            foreach ($data['styles'] as $style) {
                Storage::disk('public')->put(config('fontawesome-json-parser.export-to-route') . DIRECTORY_SEPARATOR . $style . '-' . config('fontawesome-json-parser.filename'), json_encode($icons[$style]));
            }
        }

        $this->info('File created successfully with '.count($icons).' files exported!');
    }
}
