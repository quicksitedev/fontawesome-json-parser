<?php

namespace Quicksite\FontawesomeJsonParser\Http\Controllers\JsonVisualiser;

use Illuminate\Support\Facades\Storage;

class JsonVisualiserController
{
    /**
     * Return the json datas into the view so the javascript can load it directly
     *
     */
    public function index()
    {
        $file = fopen(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/merged-icons.json'), "w");

        $families = explode(',', request()->input('family'));

        if($families[0] == '') {
            $families = ['solid'];
        }

        foreach($families as $family) {
            if(file_exists(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/'.$family.'-icons.json'))) {
                $fam = substr(
                    substr(
                        file_get_contents(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/'.$family.'-icons.json')),
                        strlen('[')
                    ),
                    0, -1
                ) . ',';

                fwrite($file, $fam);
            } else {
                $fam = substr(
                    substr(
                        file_get_contents(str_replace('Http'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR.'JsonVisualiser', '', (string) __DIR__) . 'Console/icons/'.$family.'-icons.json'),
                        strlen('[')
                    ),
                    0, -1
                ) . ',';

                fwrite($file, $fam);
            }
        }

        $stat = fstat($file);
        ftruncate($file, $stat['size']-2);
        fclose($file);

        $file = file_put_contents(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/merged-icons.json'), '[' . file_get_contents(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/merged-icons.json')));

        $file = fopen(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/merged-icons.json'), "a+");

        fwrite($file, '}]');

        return json_decode(file_get_contents(storage_path('app/public/'.config('fontawesome-json-parser.export-to-route').'/merged-icons.json')));
    }
}
