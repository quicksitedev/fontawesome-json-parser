<?php

namespace Quicksite\FontawesomeJsonParser;

use Illuminate\Support\ServiceProvider;

class FontawesomeJsonParserServiceProvider extends ServiceProvider
{
    /**
     * Boot commands, routes, helpers, translations, views
     */
    public function boot()
    {
        // Console commands
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\ProcessJsonCommand::class,
            ]);
        }

        $this->publishes([
            __DIR__ . '/../config/fontawesome-json-parser.php' => config_path('fontawesome-json-parser.php')
        ], 'config');

        $this->loadRoutesFrom(__DIR__ . '/Routes/routes.php');
    }

    /**
     * Register providers and facades
     */
    public function register()
    {
        //
    }
}
