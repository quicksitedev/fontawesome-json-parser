# Fontawesome icons library parser for Laravel

## Fontawesome

Fontawesome is a popular icons library that comes with tons of cool and fancy stuff.

For concrete examples, check out the official [website](https://fontawesome.com/).

## Purpose of this package

Fontawesome is a big library. Over 7'000 icons with the pro version. If you want to use the icons in a picker for example, loading all the informations provided when you download the icons files is too big to handle without performance issues. So, to simplify, this package extract only the basic informations and create a new, lighter file.

This package comes with the free icons. The actual version is 5.13.0. For using the pro version, please read the documentation below about to pro version.

## Install

You can install the package via Composer:

```bash
composer require quicksite/fontawesome-json-parser
```

Since Laravel 5.5 and up, the package will automatically register the service provider and facade

Next, publish the config files:

```bash
php artisan vendor:publish --provider="Quicksite\FontawesomeJsonParser\FontawesomeJsonParserServiceProvider" --tag="config"
```

## Configuration

The configuration file is simple. It will publish the files into your storage directory and the folder you define in the config file.

```php
return [

    /*
    |--------------------------------------------------------------------------
    | Fontawesome Json parser configurations
    |--------------------------------------------------------------------------
    |
    */

   'filename' => env('FONTAWESOME_JSON_FILENAME', 'icons'),

    'icons-route' => env('FONTAWESOME_JSON_ROUTE', '/fontawesome-icons'),
    'load-from-route' => env('FONTAWESOME_JSON_LOAD_FROM_ROUTE', 'fontawesome-icons'),
    'export-to-route' => env('FONTAWESOME_JSON_EXPORT_TO_ROUTE', 'fontawesome-icons'),
    'icons-route-index' => env('FONTAWESOME_ROUTE_INDEX', 'fontawesome-index'),

];

```

## Usage

### Using the free verison of Fontawesome

This package already includes the free version. To generate the files, simply run this command in your terminal:

```
    php artisan fontawesome:parse
```

This will generate separate files containing the icons for each families. For example, the free version will generate three separate files: regular, brands and solid.

Based on the config files, the generated files will be placed in your storage folder, within the folder specified in the configuration.

! Be aware that the storage disk used is:

```
    Storage::disk('local')
```

### Using the pro version of Fontawesome

If you have the pro version, simply put the icons.json file in the storage folder, within the folder specified in your configuration (json load route). If present, the artisan command will take the pro version.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
