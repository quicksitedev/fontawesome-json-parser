<?php

namespace Quicksite\FontawesomeJsonParser\Tests;

use Orchestra\Testbench\TestCase as Orchestra;
use Quicksite\FontawesomeJsonParser\FontawesomeJsonParserServiceProvider;

abstract class TestCase extends Orchestra
{
    /**
     * Get package providers.
     *
     * @param  \Illuminate\Foundation\Application  $app
     *
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            FontawesomeJsonParserServiceProvider::class
        ];
    }

    protected function getEnvironnementSetUp($app)
    {
        $app['config']->set('database.default', 'testdb');
        $app['config']->set('database.connections.testdb', [
            'driver' => 'sqlite',
            'database' => ':memory:',
        ]);
    }
}
